# frozen_string_literal: true

require 'sinatra'
require 'socket'
require 'json'

get '/' do
  erb :k8s
end

get '/hello' do
  content_type :json

  { message: 'it is working' }.to_json
end
