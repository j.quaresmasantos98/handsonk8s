ENV['APP_ENV'] = 'test'

require_relative '../app.rb'
require 'json'
require 'rspec'
require 'rack/test'

RSpec.describe 'App' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it "/hello check response" do
    get '/hello'

    expect(last_response).to be_ok
    expect(JSON.parse(last_response.body)).to eq({"message"=>"it is working"})
  end
end